//
//  ViewController.swift
//  BackgroundDownloadDemo
//
//  Created by Haldun Bayhantopcu on 17/11/14.
//  Copyright (c) 2014 monoid. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate {
  @IBOutlet weak var textView: UITextField!
  @IBOutlet weak var progressView: UIProgressView!
  
  var urlSession: NSURLSession?
  var downloadTask: NSURLSessionDownloadTask?
  var downloadPath: NSString?

  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    progressView.progress = 0.0
    progressView.hidden = true
    
    NSNotificationCenter.defaultCenter().addObserver(self,
      selector: "handleBackgroundTransfer:",
      name: "BackgroundTransferNotification", object: nil)
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }

  @IBAction func downloadButtonTapped(sender: UIButton) {
    textView.resignFirstResponder()
    progressView.hidden = false
    
    if urlSession == nil {
      let sessionIdentifier = "askdjaslakjdnajsd"
      let config = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier(sessionIdentifier)
      urlSession = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
    }
    
    let downloadURL = NSURL(string: textView.text)!
    let request = NSURLRequest(URL: downloadURL)
    downloadTask = self.urlSession?.downloadTaskWithRequest(request)
    downloadTask?.resume()
  }
  
  func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
    println("Downloaded to: \(location)")
    dispatch_async(dispatch_get_main_queue()) {
      self.progressView.hidden = true
    }
  }
  
  func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask,
    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
    totalBytesExpectedToWrite: Int64)
  {
    dispatch_async(dispatch_get_main_queue()) {
      println("totalBytesWritten: \(totalBytesWritten), totalBytesExpectedToWrite: \(totalBytesExpectedToWrite)")
    }
  }
  
  func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
    println("error: \(error)")
  }
  
  func handleBackgroundTransfer(notification: NSNotification) {
    if let userInfo = notification.userInfo {
      if let sessionIdentifier = userInfo["sessionIdentifier"] as? BackgroundTransferNotificationUserInfoValue<String> {
        NSLog("sessionIdentifier: \(sessionIdentifier)")
        dispatch_async(dispatch_get_main_queue()) {
          self.progressView.hidden = true
          if let completionHandler = userInfo["completionHandler"] as? BackgroundTransferNotificationUserInfoValue<() -> Void> {
            completionHandler.value()
          }
        }
      }
    }
  }
}

