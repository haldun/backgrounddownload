//
//  AppDelegate.swift
//  BackgroundDownloadDemo
//
//  Created by Haldun Bayhantopcu on 17/11/14.
//  Copyright (c) 2014 monoid. All rights reserved.
//

import UIKit

class BackgroundTransferNotificationUserInfoValue<T> {
  let value: T
  init(_ value: T) {
    self.value = value
  }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    return true
  }

  func application(application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: () -> Void) {
    let userInfo: [String:AnyObject] = [
      "completionHandler": BackgroundTransferNotificationUserInfoValue(completionHandler),
      "sessionIdentifier": BackgroundTransferNotificationUserInfoValue(identifier)
    ]
    NSNotificationCenter.defaultCenter().postNotificationName("BackgroundTransferNotification", object: nil, userInfo:userInfo)
  }
}

